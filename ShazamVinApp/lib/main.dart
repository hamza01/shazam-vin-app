import 'package:flutter/material.dart';
import 'screens/login.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData.dark(),
    title: 'Login Page',
    home: LoginScreen(),
  ));
}
