import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:ShazamVinApp/takepicture/vin.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _image;
  String error;
  final picker = ImagePicker();
  //la fonction qui est prendre la photo
  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('Aucune image sélectionnée.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Prenez une photo de votre Vin'),
      ),
      body: Center(
        child: _image == null
            ? Text('Aucune image de vin prise.')
            : Column(
                children: [
                  error == null ? Text("") : Text(error),
                  Image.file(_image),
                  //ce button pour valider l'image et envoye la requete au fichier server.js pour detecter le texte dans l'image
                  //et il va recuperer le vin correspondant à lequelle dans l'image
                  RaisedButton(
                    onPressed: () async {
                      var req = http.MultipartRequest(
                          "POST", Uri.parse("http://192.168.2.16/foto"));
                      req.files.add(await http.MultipartFile.fromPath(
                          "image", _image.path));
                      var resp = await req.send();
                      var respJSON = await jsonDecode(
                          String.fromCharCodes(await resp.stream.toBytes()));
                      if (respJSON.containsKey('error')) {
                        setState(() {
                          error = respJSON['error'];
                        });
                      } else {
                        //pour passer à l'image qui est contient l'affichage de la fiche technique de vin
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => Vin(respJSON)));
                      }
                    },
                    child: Text("Valider"),
                  )
                ],
              ),
      ),
      //le button pour prendre une image
      floatingActionButton: FloatingActionButton(
        //l'appel à la focntion getImage
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
