import 'package:flutter/material.dart';

class Vin extends StatefulWidget {
  var vin;
  Vin(this.vin);
  @override
  _VinState createState() => _VinState();
}

class _VinState extends State<Vin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('La fiche Technique de Vin'),
        ),
        body: Column(
          children: [
            //pour afficher la fiche technique de vin en photo
            Image.network(this.widget.vin['image']),
            Text(this.widget.vin['nom']),
            Text(this.widget.vin['description']),
            Text(this.widget.vin['nom du cepage']),
            Text(this.widget.vin['nom du chateau']),
            Text(this.widget.vin['prix']),
          ],
        ));
  }
}
