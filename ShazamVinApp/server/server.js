const vision = require('@google-cloud/vision');
const file = require('express-fileupload');
const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient; 

app.use(file());

// connexion au base donnee de Mongo
const mdb = "mongodb://127.0.0.1:27017/mydb";




app.post('/foto', async function(req,res) {
  const clientOptions = {projectId: "galvanic-botany-296920", keyFilename: './My First Project-96a5b02b7d7d.json'};

  const client = new vision.ImageAnnotatorClient(clientOptions);

  console.log("test")
  console.log(req.files.image)
  var image = req.files.image
  const filePath = './img/'+image.name;
  //pour deplacer l'image dans un autre dossier  
  image.mv(filePath, async function(err){

    const [result] = await client.textDetection(filePath);
    const labels = result.textAnnotations;
    console.log('Text:');
    let max = 0;
    let vinMatch;
    labels.forEach(label => console.log(label.description));
    MongoClient.connect(mdb, {
      useNewUrlParser: true
    }, function(err, mongoClient) {
      if (err) {
        return console.log("erreur connexion base de données");
      }
      if (mongoClient) {
        //recuperer les informations de la collection vin de MongoDb
        mongoClient.db().collection('vin').find({}).toArray( function(err, data) {
          if (err) return console.log('erreur base de données');
          if (data) {
            console.log('requete ok pour les vins');
            mongoClient.close();
            for(var i=0; i<data.length;i++){
              let matchScore = 0;
              let etiquette = data[i].etiquette;
              let etiquetteWords = etiquette.split(' ')
              for(var j =0; j<labels.length;j++){
                for(var z= 0;z<etiquetteWords.length;z++){
                  console.log("Compare "+etiquetteWords[z]+" to "+labels[j].description+" "+(labels[j].description==etiquetteWords[z]))
                  //comparer l'etiquette stocker dans notre base donner avec avec laquelle qui est detecter en image
                  if(etiquetteWords[z].toUpperCase()==labels[j].description.toUpperCase()){
                    matchScore += etiquetteWords[z].length;
                  }
                }
              }
              if(matchScore>max){
                //vinMatch c'est le vin qui va etre affifher car il contient le plus grand nombre des lettres en image
                
                max = matchScore;
                vinMatch = data[i]
              }
            }
            if(vinMatch){
              console.log("OK ",vinMatch)
              res.json(vinMatch)
            }
              
            else{
              console.log("NOT OK ",max)
              res.json({error: "Aucun resultat"})
            }
              
          }
        });
      }
    });
    
  });

})

app.listen(80);